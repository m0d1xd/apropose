package com.eb2a.flowerhomehebron.test;

import android.content.Context;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class SlideAdapter extends PagerAdapter {
    private Context mcontext;
    private LayoutInflater mLayoutInflater;
    private ArrayList<AproposeData> Data;


    public SlideAdapter(Context context, ArrayList<AproposeData> array) {
        this.mcontext = context;

        this.Data = array;
    }

    @Override
    public int getCount() {
        return Data.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == (LinearLayout) object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        mLayoutInflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = mLayoutInflater.inflate(R.layout.slide_layout, container, false);


        TextView tv1 = (TextView) view.findViewById(R.id.tv_title);
        tv1.setText(Data.get(position).getTitle());

        TextView tv2 = (TextView) view.findViewById(R.id.tv_doc);
        tv2.setText(Data.get(position).getDiscription());


        ImageView img = (ImageView) view.findViewById(R.id.img_v);

        Picasso.with(mcontext.getApplicationContext()).load(Data.get(position).getImage()).into(img);
        img.getLayoutParams().width
                = 500;
        img.getLayoutParams().height
                = 500;

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) img.getLayoutParams();
        params.gravity = Gravity.RIGHT;


        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, Object obj) {
        container.removeView((LinearLayout) obj);
    }




}
