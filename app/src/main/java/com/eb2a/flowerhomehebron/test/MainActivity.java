package com.eb2a.flowerhomehebron.test;

import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ToxicBakery.viewpager.transforms.RotateUpTransformer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ViewPager viewPager;
    private SlideAdapter mAdapter;
    private static ArrayList<AproposeData> Data = new ArrayList<>();
    String json_code = "";
    private TextView[] mDots;
    private LinearLayout mDotLayout;
    String api_url = "http://standupdisplay.tn/api/about.php";
    String TAG = "MoDi";
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);

        viewPager = (ViewPager) findViewById(R.id.vpPager);
        mDotLayout = (LinearLayout) findViewById(R.id.dotsLayout);
        progressBar = (ProgressBar) findViewById(R.id.progressBar3);

        new AsynkTaskabout().execute(api_url);
    }


    //Stream2String method
    public static String Stream2String(InputStream inputStream) {

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        String Text = "";
        try {
            while ((line = bufferedReader.readLine()) != null) {
                Text += line;
            }
            inputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return Text;
    }

    public class AsynkTaskabout extends AsyncTask<String, String, String> {

        String newData;

        @Override
        protected String doInBackground(String... params) {
            publishProgress("open connection to server ");
            try {
                URL url = new URL(params[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                publishProgress("reading data");
                newData = Stream2String(in);
                in.close();


            } catch (Exception e) {
                publishProgress("cannot connect to server");
            }

            return newData;

        }

        protected void onPostExecute(String result2) {
            Data = null;
            Data = new ArrayList<>();
            if (result2 != null) {
                json_code = newData;
                try {
                    JSONObject reader = new JSONObject(json_code);

                    //  subject.setText(Html.fromHtml(reader.getString("about")), TextView.BufferType.SPANNABLE);
                    //about.setText("About");

                    JSONArray t = reader.getJSONArray("images");
                    for (int i = 0; i < t.length(); i++) {
                        Data.add(new AproposeData());
                    }
                    for (int i = 0; i < t.length(); i++) {
                        Data.get(i).setImage(t.getString(i));

                    }

                    JSONArray t1 = reader.getJSONArray("descriptions");
                    for (int i = 0; i < t1.length(); i++) {
                        //    Description.add(t1.getString(i));
                        Data.get(i).setDiscription(t1.getString(i));
                    }

                    JSONArray t2 = reader.getJSONArray("titles");
                    for (int i = 0; i < t2.length(); i++) {
                        //   Title.add(t2.getString(i));
                        Data.get(i).setTitle(t2.getString(i));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mAdapter = new SlideAdapter(getApplicationContext(), Data);
                viewPager.setAdapter(mAdapter);
                progressBar.setVisibility(View.GONE);
                addDotsIndicator(0);
                viewPager.setPageTransformer(true, new RotateUpTransformer());
                viewPager.addOnPageChangeListener(viewListiner);

            } else {
                Log.d(TAG, "onPostExecute: Error");
                // null response or Exception occur
            }

        }
    }

    public void addDotsIndicator(int position) {
        mDotLayout.removeAllViews();
        mDots = new TextView[Data.size()];
        for (int i = 0; i < mDots.length; i++) {
            mDots[i] = new TextView(getApplicationContext());
            mDots[i].setText(Html.fromHtml("&#8226;"));
            mDots[i].setTextSize(55);
            mDots[i].setTextColor(getResources().getColor(R.color.colorTransparentWhite));
            mDotLayout.addView(mDots[i]);
        }

        if (mDots.length > 0) {
            mDots[position].setTextColor(getResources().getColor(R.color.colorWhite));
        }
    }

    ViewPager.OnPageChangeListener viewListiner = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            addDotsIndicator(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
}
